module always_comb_test(
    output first_hit,
    output multi_hit
);

always_comb begin : HIT_CHECK
    first_hit      =  0;
    multi_hit      =  0;
    out_addr       =  0;
    cache_coherent =  1;
    if(cond) begin
       cache_coherent_if =  1;
    end else begin
       cache_coherent_else =  0;
       if(cond2) out_addr = 1;
       if(cond3) begin
            out_addr = 2;
       end
    end
end
  
endmodule
